package net.canos.spring.restclient;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class RestClientIn5Minutes {

	public static void main(String[] args) {			
		RestTemplate restTemplate = new RestTemplate();
		
		String random = "http://gturnquist-quoters.cfapps.io/api/random";
    	Quote quote = restTemplate.getForObject(random, Quote.class);
    	log(quote.toString());
    	
    	String url1 = "http://gturnquist-quoters.cfapps.io/api/1";
    	
    	try {
    	restTemplate.delete(url1);
    	} catch (HttpClientErrorException e){
    		log("No se puede eliminar, no hay permisos");
    	}
    	
    	ResponseEntity<Quote> quote1 = restTemplate.getForEntity(url1,Quote.class);
    	log(quote1.getStatusCode().toString()+" "+quote1.getBody().getType().toString());
    	
	}

	private static void log(String string) {
		System.out.println(string);
	}

}
